'use strict';

const Lab = require('@hapi/lab');
const { expect } = require('@hapi/code');
const { afterEach, beforeEach, describe, it } = (exports.lab = Lab.script());
const { init } = require('../lib/server');

describe('GET /', () => {
  let server;

  beforeEach(async () => {
    try {
      server = await init();
    } catch (error) {
      throw error;
    }
  });

  afterEach(async () => {
    try {
      await server.stop();
    } catch (error) {
      throw error;
    }
  });

  it('responds with 200', async () => {
    try {
      const res = await server.inject({
        method: 'get',
        url: '/'
      });
      expect(res.statusCode).to.equal(201);
    } catch (error) {
      throw error;
    }
  });
});
