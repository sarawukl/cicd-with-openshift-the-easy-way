FROM node:10-alpine

EXPOSE 3000

COPY . /web

CMD cd /web && node index.js